#include <iostream>
#include <QAppLication>
#include <QWidget>
#include <QPixmap>
#include <QPainter>
#include <QPaintEvent>
#include <QPushButton>
#include <QMediaPlayer>
#include <QMediaContent>

class SongButton : public QPushButton {
	Q_OBJECT
    QMediaPlayer *player;

    void setUp();
    void setDown();
public:

	SongButton() = default;
	SongButton(QWidget *parent) {
		setParent(parent);
		setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        //UpButton = QPixmap("G:\\skillbox\\DZ38.5\\task1\\btn-up.PNG");
        UpButton = QPixmap(".\\btn-up.PNG");
        //DownButton = QPixmap("G:\\skillbox\\DZ38.5\\task1\\btn-down.PNG");
        DownButton = QPixmap(".\\btn-down.PNG");
        CurrentButton = UpButton;
        DownFl = false;
        setGeometry(UpButton.rect());
        connect(this, &QPushButton::clicked, this, &SongButton::changeBtn);
        downSong = QMediaContent(QUrl::fromLocalFile(".\\knopka.mp3"));
        //downSong = QMediaContent(QUrl::fromLocalFile("G:\\skillbox\\DZ38.5\\task1\\knopka.mp3"));
        player = new QMediaPlayer(parent);
	}
	void paintEvent(QPaintEvent *e) override;
    //void keyPressEvent(QKeyEvent *e) override;
	QSize sizeHint() const override;
private:
	QPixmap CurrentButton;
	QPixmap UpButton;
	QPixmap DownButton;
    QMediaContent downSong;
    /// upSong
    bool DownFl = false;
public slots:
    void changeBtn();
};

void SongButton::setUp() {
	CurrentButton = UpButton;
    DownFl = false;
    //player->setMedia(QUrl::fromLocalFile(""));
    update();
}
void SongButton::setDown() {
    CurrentButton = DownButton;
	DownFl = true;
    player->setMedia(downSong);
    player->setVolume(50);
    player->play();
    update();
}
void SongButton::changeBtn() {
    if(DownFl) setUp();
    else setDown();
}
void SongButton::paintEvent(QPaintEvent *e) {
	QPainter p(this);
	p.drawPixmap(e->rect(), CurrentButton);
}

/*void SongButton::keyPressEvent(QKeyEvent *e) {
    changeBtn();
}*/

QSize SongButton::sizeHint() const {
	return QSize(160, 160);
}

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	QWidget window;// = new QWidget;
    SongButton button(&window);
	window.resize(180, 230);
	window.move(1000, 500);

    window.show();
	app.exec();
    return 0;
}

#include "main.moc"
