
#include <QtGui/QtGui>
#include <QApplication>

//#include <QtWebEngineCore/QtWebEngineCore>
#include <QWebEngineView>
#include "ui_CustMainWind.h"
#include "CustMainWind.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    CustMainWind webEditor(nullptr);
    Ui::MainWindow caller;
    caller.setupUi(&webEditor);
    webEditor.editor = caller.plainTextEdit;
    webEditor.result = caller.webEngineView;
    webEditor.show();
    app.exec();
    return 0;
}
