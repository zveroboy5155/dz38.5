cmake_minimum_required(VERSION 3.12)
project(task_2)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

#include_directories ("G:\\Qt\\5.15.2")
#set(CMAKE_PREFIX_PATH "G:\\Qt\\5.15.2\\msvc2019_64\\lib")
set(CMAKE_PREFIX_PATH "G:\\Qt\\5.15.2\\mingw81_64\\lib")
#set(QTBUILD_STATIC_VS2019 "G:\\Qt\\5.15.2\\msvc2019_64\\lib")
#set(QTBUILD_STATIC_VS2017 "G:\\Qt\\5.15.2\\msvc2015_64")

find_package(Qt5 COMPONENTS Core Widgets WebEngineWidgets REQUIRED)

add_executable(${PROJECT_NAME} CustMainWind.h main.cpp)

target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Widgets Qt5::WebEngineCore Qt5::WebEngineWidgets)
