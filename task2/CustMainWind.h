#include <QMainWindow>
#include <QWebEngineView>
#include <QPlainTextEdit>
#include <iostream>


class CustMainWind : public QMainWindow{
    Q_OBJECT

public:
    QPlainTextEdit *editor = nullptr;
    QWebEngineView *result;
    CustMainWind() = default;
    CustMainWind(QWidget *parent): QMainWindow(parent) {};

public slots:
    void updateView(){
        std::cout <<__func__<<":" <<__LINE__<< std::endl;
        result->setHtml(editor->toPlainText());
    }
};
