
#include "photoWind.h"
#include "./ui_photoWind.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    blSlider = ui->horizontalSlider;
    pucture = ui->label;
    openBtn = ui->pushButton;
    blSlider->setMaximum(0);
    blSlider->setMaximum(10);
}

MainWindow::~MainWindow()
{
    delete ui;
}
