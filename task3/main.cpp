
#include "photoWind.h"
#include <QApplication>

#include <QGraphicsScene>
#include <QGraphicsBlurEffect>
#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QFileDialog>
//#include<QDir>

QImage blurImage(QImage source, int blurRadius){

    if(source.isNull()) return QImage();

    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    auto *blur =new QGraphicsBlurEffect;
    QImage res(source.size(), QImage::Format_ARGB32);
    QPainter painter(&res);

    item.setPixmap(QPixmap::fromImage(source));
    blur->setBlurRadius(blurRadius);
    item.setGraphicsEffect(blur);
    scene.addItem(&item);
    res.fill(Qt::transparent);

    scene.render(&painter, QRectF(),
                 QRect(0, 0, source.width(), source.height()));
    return res;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    QImage curPict;
    //QString filePath;
    QObject::connect(w.openBtn, &QPushButton::clicked, [&w, &curPict](){

        QString filePath = QFileDialog::getOpenFileName(nullptr,
                                                "Open pucture file",
                                                "G:\\",
                                                "PNG files, JPG files (*.png, *.PNG, *jpg)");
        curPict = QImage(filePath);

        w.pucture->setPixmap(QPixmap::fromImage(blurImage(curPict, 0).scaled(
            w.pucture->width(),
            w.pucture->height(), Qt::KeepAspectRatio)));
    });

    QObject::connect(w.blSlider, &QSlider::valueChanged, [&w, &curPict](int newVal){

        w.pucture->setPixmap(QPixmap::fromImage(blurImage(curPict, newVal).scaled(
            w.pucture->width(),
            w.pucture->height(), Qt::KeepAspectRatio)));
        //blurImage( pictur, newVal);
    });
    w.show();
    a.exec();
    return 0;
}
