
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSlider>
#include <QLabel>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow

{
    Q_OBJECT

public:
    QSlider *blSlider;
    QLabel *pucture;
    QPushButton *openBtn;

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void test_fn();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
